import argparse
import logging
import os

from jinja2 import Environment, FileSystemLoader


logger = logging.getLogger('Docker image creation script')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

logger.addHandler(ch)


def update_dockerfile(base, maintainer, packages, docker_dir="."):
    env = Environment(
        loader=FileSystemLoader(docker_dir),
    )
    dockerfile = env.get_template("Dockerfile_template")

    final_dockerfile = dockerfile.render(
        base=base,
        maintainer=maintainer,
        packages=packages
    )

    with open("Dockerfile", "w") as df:
        df.write(final_dockerfile)


def build_docker(base, maintainer, packages):
    logger.info("Building docker image")

    update_dockerfile(base, maintainer, packages)

    command = "docker build -t create-dockerfile-image . "

    ret_status = os.system(command)

    if ret_status:
        msg = "Cannot build an image"
        logger.debug(msg)
        raise Exception("Cannot build an image")


def tag_docker(name, tag):
    logger.info("Tagging docker")
    command = "docker tag create-dockerfile-image {name}:{tag}".format(name=name, tag=tag)
    ret_status = os.system(command)

    if ret_status:
        raise Exception("Cannot tag the image")

    return ret_status


if __name__ == '__main__':
    logger.info("Starting up an app")

    parser = argparse.ArgumentParser(description="Script for creating docker image")
    parser.add_argument('--base-img', type=str, dest='base', help="Base image", choices=[
        'centos', 'ubuntu', 'debian', 'alpine'
        ],
        required=True
    )
    parser.add_argument('-n', '--name', help="name of the image", required=True)
    parser.add_argument('-t', '--tag', type=str, help="tag for building/push (default is: latest)", required=False, default='latest')
    parser.add_argument('-m', '--maintainer', type=str, help="creator user/mail to contact", required=True)
    parser.add_argument('-p', '--list', dest='packages', nargs='+', help='Additional packages', required=False)

    args = parser.parse_args()

    build_docker(
        base=args.base,
        maintainer=args.maintainer,
        packages=args.packages
    )

    tag_docker(
        name=args.name,
        tag=args.tag
    )
