# Templating tool

## Requirements

 * Jinja2

## Installation

```
pip install -r requirements.txt
```


## How to Run

```bash
python create_dockerfile.py
```
